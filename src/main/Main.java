package main;

import dbconnector.Connector;
import essence.Bolids;
import essence.Pilots;
import formula1.Formula1;
import org.xml.sax.SAXException;
import parsers.MyDOMParser;
import parsers.MySAXParser;
import servers.Server;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String[] args){

        final String fileXml = "Resources/Formula 1.xml";
        final String fileXsd = "Resources/Formula1.xsd";

        final String DB_NAME = "F1";
        final String USER = "postgres";
        final String PASS = "postgres";

        Formula1 formula1FromXML = new Formula1();

        try{

//            parseSAX(formula1FromXML, fileXml, fileXsd);
//            parseDOM(formula1FromXML, fileXml, fileXsd);

            Server server = new Server(8080, DB_NAME, USER, PASS);
            server.startServer();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void parseSAX(Formula1 formula1FromXML, String fileXML, String fileXSD) throws SAXException, IOException, ParserConfigurationException {

        MySAXParser parser = new MySAXParser(formula1FromXML, fileXML,
                fileXSD);
        parser.inizializate();
        parser.parse();
    }

    public static void parseDOM(Formula1 formula1FromXML, String fileXML, String fileXSD) {

        MyDOMParser parserDom = new MyDOMParser(formula1FromXML, fileXML, fileXSD);
        parserDom.parse();

    }
}
