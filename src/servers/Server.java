package servers;

import dbconnector.Connector;
import essence.Bolids;
import essence.Pilots;
import formula1.Formula1;

import java.net.*;
import java.io.*;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;

public class Server {

    private final ServerSocket serverSocket;
    private Socket socketClient;

    private String typeReq;
    private String fileReq;

    private OutputStream outS;
    private InputStream inS;

    private String response;



    private Connector connector;
    private Formula1 formula1FromDB = new Formula1();

    public Server(int port, String nameDB, String userDB, String passDB) throws IOException {

        serverSocket = new ServerSocket(port);

        connector = new Connector(nameDB, userDB, passDB);
    }

    public void startServer() {

        while (true) {
            waiteConnection();
            readRequest();

            generateResponse();
        }
    }

    private void waiteConnection() {

        try {
            this.socketClient = this.serverSocket.accept();
        }catch (Exception ex){
            waiteConnection();
        }
    }

    private void readRequest() {
        try {
            inS = this.socketClient.getInputStream();
            this.outS = this.socketClient.getOutputStream();

            byte[] buffer = new byte[64 * 1024];
            int r = inS.read(buffer);
            String request = new String(buffer, 0, r);

            typeReq = request.substring(0, request.indexOf("/")-1);

            int indexRef = request.indexOf("/") + 1;
            int lastRef = request.indexOf("HTTP", indexRef) - 1;
            fileReq = request.substring(indexRef, lastRef);
        }
        catch (Exception ex){
            waiteConnection();
        }
    }

    private void generateResponse() {

        if(typeReq.equals("GET")) {
            getRequest();
        }
    }

    private void getRequest(){

        try {
            if (fileReq.equals("") || fileReq.equals("index.php") || fileReq.equals("index")) {
                fileReq = "index.html";
            }

            File file = new File("web/"+fileReq);

            System.out.println(typeReq + " -> " + fileReq);

            if (!file.exists()) {
                get404();
            } else {
                get200(file);
            }

            inS.close();
            outS.close();

            if(!this.socketClient.isClosed()) {
                this.socketClient.close();
            }

            typeReq = "";
        } catch (Exception ex) {
            typeReq = "";
        }
    }

    private void get404() throws IOException {

        byte[] bufferFile = new byte[64 * 1024];

        response = "HTTP/1.1 404 Not Found\n";
        response += "Date: " + LocalDate.now() + "\n";
        response += "Content-Type: text/html\n";
        response += "Connection: close\n";
        response += "Server: Server\n";
        response += "Pragma: no-cache\n\n";

        outS.write(response.getBytes());

        FileInputStream fis = new FileInputStream("web/404.html");
        int write = 1;
        while (write > 0) {
            write = fis.read(bufferFile);
            if (write > 0) outS.write(bufferFile, 0, write);
        }

        fis.close();
    }

    private void get200(File file) throws IOException, SQLException, ClassNotFoundException {

        byte[] bufferFile = new byte[64 * 1024];

        response = "HTTP/1.1 200 OK\n";
        response += "Last-Modified: " + new Date(file.lastModified()) + "\n";
        response += "Content-Length: " + file.length() + "\n";
        response += "Content-Type: ";

        switch (fileReq.substring(fileReq.indexOf(".")+1).toLowerCase()){
            case "html":
                response += "text/html" + "\n";
                break;
            case "css":
                response += "text/css" + "\n";
                break;
            case "ico":
                response += "image/vnd.microsoft.icon" + "\n";
                break;
            case "png":
                response += "image/png" + "\n";
                break;
            case "jpg":
                response += "image/jpeg" + "\n";
                break;
            case "jpeg":
                response += "image/jpeg" + "\n";
                break;
            case "min.js":
                response += "text/javascript" + "\n";
                break;
            case "js":
                response += "text/javascript" + "\n";
                break;
            default:
                response += response += "text/html" + "\n";
        }

        response += "Connection: close\n";
        response += "Server: Server\n\n";

        outS.write(response.getBytes());

        String reqUri = fileReq.substring(0, fileReq.indexOf(".")).toLowerCase();
        String nameTable = "";

        switch (reqUri){
            case "pilots":
                nameTable = "Pilots";
                break;
            case "bolids":
                nameTable = "Bolids";
                break;
        }
        if(!nameTable.equals("")){
            connector.connect();

            connector.querySelectAndInsert("select * from \""+ nameTable +"\";",
                    formula1FromDB, nameTable);

            connector.closeConnection();

            switch (reqUri){
                case "pilots":
                    generatePilotsHtml();
                    break;
                case "bolids":
                    generateBolidsHtml();
                    break;
            }

        }
        FileInputStream fis = new FileInputStream("web/" + fileReq);
        int write = 1;
        while (write > 0) {
            write = fis.read(bufferFile);
            if (write > 0) outS.write(bufferFile, 0, write);
        }

        fis.close();

        outS.close();
    }

    private void generatePilotsHtml() throws IOException {

        String html = "<meta charset=\"UTF-8\"><style>\n" +
                ".wrapper{\n" +
                "    display: flex;\n" +
                "    flex-wrap: wrap;\n" +
                "    justify-content: space-between;\n" +
                "}\n" +
                ".wrapper .elem{\n" +
                "    display: inline-block;\n" +
                "}\n" +
                "table {\nfont-family: \"Lucida Sans Unicode\", \"Lucida Grande\", Sans-Serif;\n" +
                "    text-align: center;\n" +
                "    border-collapse: collapse;\n" +
                "    border-spacing: 0px;\n" +
                "    background: #ECE9E0;\n" +
                "    color: #656665;\n" +
                "    border: 20px solid #ECE9E0;\n" +
                "    border-radius: 20px;"+
                "}\n" +
                "th {\n" +
                "    font-size: 18px;\n" +
                "    padding: 10px;\n" +
                "}\n" +
                "td {\n" +
                "    background: #F5D7BF;\n" +
                "    padding: 10px;\n" +
                "}\n" +
                "tr:hover{\n" +
                "    color: white;\n" +
                "    background-color: black;\n" +
                "}\n" +
                "td:hover{\n" +
                "    color: white;\n" +
                "    background-color: black;\n" +
                "}"+
                "</style>";

        html += "<div class=\"wrapper\">" +
                "<div class=\"elem\">\n" +
                "  <table class=\"table-pilots\" border=\"1\">\n" +
                "      <tr>\n" +
                "        <th>Личный номер пилота</th>\n" +
                "        <th>Имя пилота</th>\n" +
                "        <th>Фамилия пилота</th>\n" +
                "        <th>Дата рождения</th>\n" +
                "      </tr>\n" + formula1FromDB.pilotsToHtmlString() +
                "</table>\n" +
                "</div>" +
                "</div>";

        FileWriter fileWriter = new FileWriter("web/Pilots.html");
        fileWriter.write(html);
        fileWriter.close();
    }

    private void generateBolidsHtml() throws IOException {
        String html = "<meta charset=\"UTF-8\"><style>\n" +
                ".wrapper{\n" +
                "    display: flex;\n" +
                "    flex-wrap: wrap;\n" +
                "    justify-content: space-between;\n" +
                "}\n" +
                ".wrapper .elem{\n" +
                "    display: inline-block;\n" +
                "}\n" +
                "table {\nfont-family: \"Lucida Sans Unicode\", \"Lucida Grande\", Sans-Serif;\n" +
                "    text-align: center;\n" +
                "    border-collapse: collapse;\n" +
                "    border-spacing: 0px;\n" +
                "    background: #ECE9E0;\n" +
                "    color: #656665;\n" +
                "    border: 20px solid #ECE9E0;\n" +
                "    border-radius: 20px;"+
                "}\n" +
                "th {\n" +
                "    font-size: 18px;\n" +
                "    padding: 10px;\n" +
                "}\n" +
                "td {\n" +
                "    background: #F5D7BF;\n" +
                "    padding: 10px;\n" +
                "}\n" +
                "tr:hover{\n" +
                "    color: white;\n" +
                "    background-color: black;\n" +
                "}\n" +
                "td:hover{\n" +
                "    color: white;\n" +
                "    background-color: black;\n" +
                "}"+
                "</style>";

        html += "<div class=\"wrapper\">" +
                "<div class=\"elem\">\n" +
                "  <table class=\"table-bolids\" border=\"1\">\n" +
                "      <tr>\n" +
                "        <th>Название болида</th>\n" +
                "        <th>Название движка</th>\n" +
                "        <th>Название шасси</th>\n" +
                "        <th>Год болида</th>\n" +
                "      </tr>\n" + formula1FromDB.bolidsToHtmlString() +
                "  </table>\n" +
                "</div>" +
                "</div>";
        FileWriter fileWriter = new FileWriter("web/Bolids.html");
        fileWriter.write(html);
        fileWriter.close();
    }
}
