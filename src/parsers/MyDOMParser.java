package parsers;

import essence.Bolids;
import essence.Pilots;
import formula1.Formula1;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyDOMParser {

    private String fileNameXml;
    private String fileNameXsd;

    private final Formula1 formula1;
    private final List<String> namesClass;

    public MyDOMParser (Formula1 formula1, String fileNameXml, String fileNameXsd){
        this.fileNameXml = fileNameXml;
        this.fileNameXsd = fileNameXsd;
        this.formula1 = formula1;
        this.namesClass = formula1.getClassNames();
    }

    public boolean parse()
    {
        try
        {
            File xmlFile = new File(this.fileNameXml);
            File xsdFile = new File(this.fileNameXsd);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(xsdFile);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setSchema(schema);
            factory.setNamespaceAware(true);

            DocumentBuilder builder = factory.newDocumentBuilder();

            Document doc = builder.parse(xmlFile);
            doc.getDocumentElement().normalize();

            for (String nameClass : namesClass) {
                NodeList listNodes = doc.getElementsByTagName(nameClass);

                for (int i = 0; i < listNodes.getLength(); i++) {
                    Node node = listNodes.item(i);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;

                        switch (element.getNodeName()) {
                            case "Pilots":

                                formula1.addNewPilot(Short.parseShort(element.getElementsByTagName("Personal_pilot_number").item(0).getTextContent()),
                                        element.getElementsByTagName("Name").item(0).getTextContent(),
                                        element.getElementsByTagName("Surname").item(0).getTextContent(),
                                        LocalDate.parse(element.getElementsByTagName("Date_of_birth").item(0).getTextContent(),
                                                DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                                break;
                            case "Bolids":
                                formula1.addNewBolid(element.getElementsByTagName("Name_bolid").item(0).getTextContent(),
                                        element.getElementsByTagName("Name_engine").item(0).getTextContent(),
                                        element.getElementsByTagName("Name_chassis").item(0).getTextContent(),
                                        Year.parse(element.getElementsByTagName("Year_bolid").item(0).getTextContent(),
                                                DateTimeFormatter.ofPattern("yyyy")));
                                break;
                        }
                    }
                }
            }

            return true;

        } catch (ParserConfigurationException | SAXException | IOException ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
}
