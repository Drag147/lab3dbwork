package essence;

public interface IExportHelperString {

    String toString();

    String toHtmlTableString();

    String toJson();
}
