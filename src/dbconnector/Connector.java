package dbconnector;

import formula1.Formula1;

import java.sql.*;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Connector {

    private String DB_URL = "jdbc:postgresql://127.0.0.1:5432/";
    private final String USER; //= "postgres";
    private final String PASS; //= "postgres";

    private Connection connection;

    public Connector (String nameDd, String user, String password){
        this.DB_URL = this.DB_URL.concat(nameDd);
        this.USER = user;
        this.PASS = password;
    }

    public void connect () throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");

        this.connection = DriverManager.getConnection(DB_URL, USER, PASS);
    }

//    public List<String> querySelect(String queryString, List<String> nameFields) throws SQLException {
//
//        List<String> paramsTmp = new LinkedList<>();
//        try{
//            Statement statement =  this.connection.createStatement();
//
//            ResultSet result1 = statement.executeQuery(queryString);
//
//            while (result1.next()) {
//                //System.out.println("Номер строки #" + result1.getRow());
//                for (int i = 1; i < result1.getFetchSize()+1; i++) {
//                    //System.out.println("\t" + nameField + " : " + result1.getString(nameField));
//                    paramsTmp.add(result1.getString(i));
//                }
//            }
//
//            statement.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            throw e;
//        }
//    }

    public void querySelectAndInsert(String queryString, Formula1 formula1, String nameClass) throws SQLException {

        Statement statement =  this.connection.createStatement();
        ResultSet result1 = statement.executeQuery(queryString);
        try{
            switch (nameClass) {
                case "Pilots":
                    while (result1.next()) {
                        formula1.addNewPilot(result1.getShort(1), result1.getString(2),
                                result1.getString(3),
                                LocalDate.parse(result1.getString(4), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    }


                    break;
                case "Bolids":
                    while (result1.next()) {
                        formula1.addNewBolid(result1.getString(1), result1.getString(2),
                                result1.getString(3),
                                Year.parse(result1.getString(4), DateTimeFormatter.ofPattern("yyyy")));
                    }

                    statement.close();

                    break;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        finally {
            statement.close();
        }
    }

    public void closeConnection() throws SQLException {
        this.connection.close();
    }
}
